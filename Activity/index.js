const express = require("express");
const mongoose = require("mongoose");
const port = 3001;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect(
    "mongodb+srv://admin123:admin123@cluster0.wqnqsck.mongodb.net/s35?retryWrites=true&w=majority", { 
        useNewUrlParser: true, 
        useUnifiedTopology: true 
});

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    status: {
        type: String,
        default: "pending",
    },
});

const User = mongoose.model("User", userSchema);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("Connected to MongoDB Atlas!"));

app.post("/signup", (req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if (result != null && result.username == req.body.username) {
            res.send("Duplicate found!");
        } else {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((saveErr, saveUser) => {
                if (saveErr) {
                    return console.log(saveErr);
                } else {
                    return res.status(201).send("New user registered");
                };
            })
        };
    });
});

app.listen(port, () => console.log(`Server running at port ${port}`));